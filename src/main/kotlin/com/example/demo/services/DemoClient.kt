package com.example.demo.services

import com.example.demo.grpc.Demo
import com.example.demo.grpc.DemoServiceGrpc
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class DemoClient {

    private val channel: ManagedChannel by lazy {
        ManagedChannelBuilder.forTarget("localhost:6565")
            .usePlaintext()
            .build()
    }

    @Scheduled(fixedDelay = 3000L)
    fun run() {
        val stub = DemoServiceGrpc.newBlockingStub(channel)
        val dto = Demo.DemoDTO.newBuilder()
            .setId(System.currentTimeMillis())
            .setName("test")
            .build()
        stub.show(dto)
    }
}