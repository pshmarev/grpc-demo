package com.example.demo.services

import com.example.demo.grpc.Demo
import com.example.demo.grpc.DemoServiceGrpc
import com.google.protobuf.Empty
import io.grpc.stub.StreamObserver
import org.lognet.springboot.grpc.GRpcService
import org.slf4j.LoggerFactory

@GRpcService
class DemoService : DemoServiceGrpc.DemoServiceImplBase() {

    private val log = LoggerFactory.getLogger(DemoService::class.java)

    override fun show(request: Demo.DemoDTO, responseObserver: StreamObserver<Empty>) {
        log.info("request $request")

        // demonstrates how to work with optional fields
        // all optional fields have method 'has<fieldName>', but mandatory fields don't have such a method
        log.info("request hasName=${request.hasName()}, hasCargo=${request.hasCargo()}")

        responseObserver.onNext(Empty.newBuilder().build())
        responseObserver.onCompleted()
    }


}